Docker images for amd64 and arm64 are available from hub.docker.com:

    docker pull wjcarpenter/tsduck-proxy

TSDuck, The MPEG Transport Stream Toolkit, is quite a powerful and
flexible toolkit. https://tsduck.io/. This docker image uses TSDuck on
top of Ubuntu server to create a simple mpegts proxy. If you want to
build the docker image locally, you can use the Dockerfile. If you
want to run the image in a container, you can use the
docker-compose.yml file; you will have to modify at least one
environment variable when creating the container.

Why did I make this? For a particular project and hardware
environment, my mpegts source created streams that were not usable
directly by my mpegts sink. In particular, the source would signal EOF
and start a new stream at inconvenient times. The arguments I selected
for TSDuck's tsp command do a couple of things. First, when it sees
EOF (or maybe other kinds of errors), it re-opens the source stream
and keeps on proxying to the sink. Second, it sets up a simple
streaming server that will repeatedly serve requests. It's not a
general purpose streaming server and can only serve a single request
at a time, but that was sufficient for my use case. After sending the
stream through this proxy, it was acceptable to my mpegts
consumer.

Since the proxy does not do any transcoding or other
resource-intensive operations, it's reasonably efficient and just acts
as an mpegts packet shuttle. I run it on a Raspberry Pi 4, and it only
consumes a tiny percentage of CPU. I'm not completely sure I can even
measure it. The defaults configure a small global buffer size (see
"--buffer-size-mb" and "--max-flushed-packets") to minimize useless
packets accumulated while nothing is reading them. TSDuck docs warn
that this is a trade-off of latency versus resource usage. I haven't
found it to make a significant difference in this use case.

The defaults in the docker image and compose file do what I wanted,
but the whole tsp command line is controlled by container environment
variables, so you can make it do something slightly or completely
different just by changing the values of those variables.

Note: The defaults configure the "history" plugin, which reports
happenings on stderr to show up in the container's log file. It is
normal to see it report an error when a consumer disconnects from
the proxy output port.
