#!/bin/bash
set -x
tsp --version
tsp \
    ${TSP_GLOBAL_ARGS} \
    ${TSP_INPUT_ARGS} ${TSP_INPUT_URL} \
    ${TSP_PROCESSING_ARGS} \
    ${TSP_OUTPUT_ARGS} ${TSP_OUTPUT_SERVER}
