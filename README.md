# tv scraps

This is a collection of scripts, data files, and other odds and ends related to various TV-related projects. Where it matters, my location is in the Seattle area of USA.

## Philo channels not available on TV Everywhere

I did a little screen scraping to get the full list of Philo channels.
This is everything, including their add-on packages, that is not available (to me) on TVE.
FYI, I am on the US west coast (near Seattle).
Some Philo channel feeds are East/West specific.
I got the complete list of Philo channels by grabbing and manipulating the text behind https://www.philo.com/go/channels?ref=www.philo.com

The IDs are what you need to know for the TVE alternative techniques that have been discussed over the last year or two in these Channels DVR forums.
* [HDMI for Channels](https://community.getchannels.com/t/hdmi-for-channels/36302)
* [ADBTuner: A "channel tuning" application for networked Google TV / Android TV devices](https://community.getchannels.com/t/adbtuner-a-channel-tuning-application-for-networked-google-tv-android-tv-devices/36822).
For example, Catchy Comedy is `https://www.philo.com/player/player/channel/Q2hhbm5lbDo2MDg1NDg4OTk2NDg0Mzk2OTU` (notice the doubled `player`).

The list is in two forms.
* philo-not-tve.txt: a simple list of channel names and IDs
* philo-not-tve.json: a JSON file suitable for import into ADBTuner configuration

For the JSON file, I used channel numbers starting at 77001
(or you can let Channels assign channel numbers).

I added Gracenote `tvc_guide_stationid` where I could figure them out.
I used the Channels DVR query `http://your.channels.server:8089/tms/stations/catchy`.
In many cases, that returned multiple line-ups
and I had to choose one based on scant information.
I generally chose entries with "HDTV", "en", and "streaming" tags,
but there was a lot of variety.
If you spot any errors in those Gracenote values, please let me know.

## TSDuck proxy

More details in tsduck-proxy/README.txt. 
That directory is the inputs to creating a docker image that uses TSDuck to create a simple mpegts proxy.
It solves a particular problem I had, and you might find it useful, too, for my use case or for others.
The docker images that I publish are available for both amd64 and arm64, but you could use the inputs to build for other architectures.
